
require 'elasticsearch'
require 'io/console'
require 'json'
require 'logger'

module Helpers

ElasticsearchURL =
  'https://022d92a4ba7ff6fdacc2a7182948cb0a.us-central1.gcp.cloud.es.io:9243'
ElasticsearchUser ='elastic'
ElasticsearchHost =
  '022d92a4ba7ff6fdacc2a7182948cb0a.us-central1.gcp.cloud.es.io'
ElasticsearchPort = 9243

ElasticsearchConfig = {
  host: {
    scheme: 'https',
    user: ElasticsearchUser,
    password: nil,
    host: ElasticsearchHost,
    port: ElasticsearchPort,
  },
  log: false,
}

SidekiqIndex = 'pubsub-sidekiq-inf-gprd-2019.09.05'

def initialize_log
  STDOUT.sync = true
  log = Logger.new(STDOUT, level: Logger::INFO)
  log.formatter = proc do |level, t, name, msg|
    "%s %-5s %s\n" % [ t.strftime('%Y-%m-%d %H:%M:%S'), level, msg ]
  end
  log
end

def log
  @log ||= initialize_log
end

def password_prompt(prompt='Enter elastic.co password: ')
  begin
    $stdout.write(prompt)
    $stdout.flush
    $stdin.noecho(&:gets).chomp
  ensure
    $stdin.echo = true
    $stdout.write "\r\n"
    $stdout.ioflush
  end
end

def elasticsearch_client
  return @elasticsearch_client unless @elasticsearch_client.nil?
  host = ElasticsearchConfig.delete :host
  password = ENV.fetch('ELASTIC_PASSWORD', nil)
  if password.nil? or password.empty?
    log.warn "No ELASTIC_PASSWORD variable set in environment"
    password = password_prompt
    abort "Cannot proceed without a password." if password.empty?
  end
  host[:password] = password
  ElasticsearchConfig.merge!({ hosts: [ host ] })
  @elasticsearch_client = Elasticsearch::Client.new ElasticsearchConfig
end

def index_exists?(_index=SidekiqIndex)
  __index = { :index => _index }
  elasticsearch_client.indices.exists?(arguments = __index)
end

end # module Helpers
