#! /usr/bin/env ruby

project = File.expand_path(File.dirname(__FILE__))
$LOAD_PATH.unshift project unless $LOAD_PATH.include? project

require 'helpers'
require 'optparse'

Options = {
  sample: false,
  save: "./results-#{Time.now.strftime('%Y-%m-%d_%H%M%S')}.json",
  no_save: false,
  query: {
    from: 0,
    size: 10,
    sort: [ { '@timestamp': { order: 'desc' } } ],
    query: {
      bool: {
        must: [
          { query_string: { fields: ['message'], query: '*' } },
        ],
      },
    },
  },
  index: "pubsub-sidekiq-inf-gprd-#{Time.now.strftime('%Y.%m.%d')}*",
  type: 'doc',
  size: Float::INFINITY,
  truncate: Float::INFINITY,
  pluck: nil,
  count_only: false,
  log_level: Logger::INFO,
}

def parse_args
  opt = OptionParser.new
  opt.banner = "Usage: #{$PROGRAM_NAME} [options] --current-file-server <servername> --target-file-server <servername>"
  opt.separator ''
  opt.separator 'Options:'

  opt.on('-l', '--list', "List indices") do |list|
    Options[:list] = true
  end

  opt.on('-i', '--indices', "List indices") do |list|
    Options[:list] = true
  end

  opt.on('-q', '--query=path', "Path to a file with a lucene query in json format") do |path|
    Options[:query] = path
  end

  opt.on('-I', '--index=name', "Search a specific index") do |index_name|
    Options[:index] = index_name
  end

  opt.on('-t', '--type=name', "Search for a specific record type") do |type|
    Options[:type] = type
  end

  opt.on('-h', '--hits-only', "Show hits only; no post-processing") do |hits_only|
    Options[:hits_only] = true
  end

  opt.on('-s', '--save=path', "Path to which search results will be saved in json format") do |path|
    Options[:save] = path
  end

  opt.on('-S', '--no-save', "Skip presistence") do |no_save|
    Options[:no_save] = true
  end

  opt.on('-n', '--limit=N', Integer, "Define query limit") do |limit|
    Options[:size] = limit
  end

  opt.on('-T', '--truncate=N', Integer, "Truncate results to N entries") do |truncate|
    Options[:truncate] = truncate
  end

  opt.on('-p', '--pluck=field', String, "Collect only values for the given field") do |field|
    Options[:pluck] = field
  end

  opt.on('-u', '--uniq', "Filter duplicate values") do |uniq|
    Options[:uniq] = true
  end

  opt.on('-c', '--count', "Just count the results") do |count|
    Options[:count_only] = true
  end

  opt.on('-s', '--sample', 'Show a sample of the results before processing') do |sample|
    Options[:sample] = true
  end

  opt.on('-o', '--order=<asc/desc>', 'Order in which to sort results; default: desc') do |arg|
    if (order,_ = arg.match(/^(asc|desc)(ending)?$/i).captures)
      Options[:order] = order
    else
      STDERR.puts "Invalid argument: #{arg}"
      STDERR.puts opt
      exit
    end
  end

  opt.on_tail('-q', '--quiet', 'Silence logging') do |verbose|
    Options[:quiet] = true
  end

  opt.on_tail('-v', '--verbose', 'Increase logging verbosity') do |verbose|
    Options[:log_level] -= 1
  end
  opt.on_tail('-?', '--help', 'Show this message') do
    puts opt
    exit
  end
  begin
    args = opt.order!(ARGV) {}
    opt.parse!(args)
  rescue OptionParser::InvalidOption => e
    puts e.message
    puts opt
    exit
  end
  Options
end

class Searcher
  include Helpers

  def search(query, _index=Options[:index], _type=Options[:doc])
    options = {
      index: _index,
      type: _type,
      body: query,
    }
    results = {}
    begin
      log.debug "Searching index: #{_index}" unless Options[:quiet]
      results = elasticsearch_client.search(options)
    rescue Elasticsearch::Transport::Transport::Errors::NotFound => e
      error = JSON.parse(e.message[6..-1]).fetch('error', {})
      reason = error.fetch('reason', 'unknown')
      index_name = error.fetch('index', 'unknown')
      log.error "Search failed: #{reason}: #{index_name}" unless Options[:quiet]
    end
    results.fetch('hits', {}).fetch('hits', [])
  end

  def list_indices
    indices = elasticsearch_client.indices.get_settings.sort.to_h
    for index_name, index_settings in indices
      log.info index_name
    end
  end

  def main
    args = parse_args
    log.level = args[:log_level]
    log.formatter = proc { |level, t, name, msg| "#{msg}\n" } if Options[:quiet]

    if Options[:list]
      list_indices
      exit
    end

    query = Options[:query]
    query = JSON.parse(File.read(query)) if query.is_a?(String) && File.exists?(query)
    if Options[:size] < Float::INFINITY && query.include?(:size)
      query[:size] = Options[:size]
    end
    if Options[:order] && query.include?(:sort)
      query[:sort].each_with_index do |sorting, i|
        for key,value in sorting
          if value.include? :order
            query[:sort][i][key][:order] = Options[:order]
          end
        end
      end
    end
    hits = search query

    log.info "Found #{hits.length} hits" unless Options[:quiet]
    exit unless hits.length > 0

    if Options[:truncate] < Float::INFINITY
      hits = hits[0...Options[:truncate]]
    end

    if Options[:hits_only]
      results = JSON.pretty_generate(hits)
      log.info results
      unless Options[:no_save]
        File.open(Options[:save], 'w') { |f| f.write(results) }
        log.info "Persisted #{hits.length} records" unless Options[:quiet]
      end
      exit
    end

    if Options[:sample]
      log.info "  Sample:" unless Options[:quiet]
      log.info JSON.pretty_generate(hits.first.fetch('_source', {}).fetch('json', {}))
      exit
    end

    records = []
    plucked_values = Set.new
    plucked_value_counts = Hash.new
    for hit in hits
      data = hit.fetch('_source', {}).fetch('json', {})
      if (field_to_pluck = Options[:pluck])
        value = data.fetch(field_to_pluck, 'none')
        if Options[:count_only]
          plucked_value_counts[value.intern] ||= 0
          plucked_value_counts[value.intern] += 1
        elsif Options[:uniq]
          plucked_values << value
        else
          record = {field.intern => value}
        end
      else
        record = {
          data: data.fetch('message'),
        }
        if (args = data['args'])
          record.merge!({project_id: args.first})
        end
        if (_retry = data['retry'])
          record.merge!({retry: _retry})
        end
        if (error_backtrace = data['error_backtrace'])
          record.merge!({backtrace: error_backtrace})
        end
        if (error_class = data['error_class'])
          record.merge!({error: error_class})
        end
      end
      records.append record
    end
    if plucked_value_counts.length > 0
      results = JSON.pretty_generate(plucked_value_counts)
      log.info results
    elsif records.length > 0
      results = JSON.pretty_generate(records)
      log.debug results
      unless Options[:no_save]
        File.open(Options[:save], 'w') { |f| f.write(results) }
        log.info "Persisted #{hits.length} records" unless Options[:quiet]
      end
    end
  rescue StandardError => e
    log.error "Unexpected error: #{e}"
    e.backtrace.each { |t| log.error t }
  end

end

Searcher.new.main if $PROGRAM_NAME == __FILE__
