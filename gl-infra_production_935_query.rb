{
    "from": 0,
    "size": 10000,
    "sort": [
        {
            "@timestamp": {
                "order": "desc"
            }
        }
    ],
    "query": {
        "bool": {
            "must": [
                {
                    "query_string": {
                        "query": "json.class: HashedStorage AND ProjectMigrateWorker"
                    }
                },
                {
                    "query_string": {
                        "query": "json.job_status: fail"
                    }
                },
                {
                    "query_string": {
                        "query": "json.retry_count: 2"
                    }
                },
                {
                    "query_string": {
                        "query": "_exists_:json.error_class"
                    }
                }
            ]
        }
    }
}