# query-elasticsearch-rb

This is a simple script for configuring access and running lucene queries 
against a target elasticsearch host and index.

## Install

Installation is just cloning the git repository and then installing the 
project dependencies using `bundler`.

```{sh}
git clone git@gitlab.com:nnelson/query-elasticsearch-rb.git
cd query-elasticsearch-rb
bundler install --path=./vendor/bundle
bundler update
```

## Setup

Before running queries against GitLab's elk indices, a credential must be 
set for an authorized user.

```{sh}
export ELASTIC_PASSWORD='CHANGEME'
```

Currently this user is `elastic`.  The password can be found in the 1Password 
`production` vault, in the `Elastic Cloud` login, under the entry for 
`gitlab-production password`.

## Usage

Please read the full usage with `bundler exec ./query.rb --help`.  Here are 
a handful of useful examples.

Run the default query, with the default results size limit of 10, on the
default index.

```{sh}
bundler exec ./query.rb --hits-only --no-save --quiet | jq
```

Produce a single sample document of the latest sidekiq index, without saving 
any of the results.

```{sh}
bundler exec ./query.rb --sample --no-save
```

List al of the indices available for searching.

```{sh}
bundler exec ./query.rb --indices
```

Just list the `sidekiq` indices.

```{sh}
bundler exec ./query.rb --indices | grep sidekiq
```

Search all sidekiq indices for a specific day, and save all records to a 
specific json formatted output file.

```{sh}
export now=$(date +'%Y-%m-%d_%H%M%S')
bundler exec ./query.rb --index=pubsub-sidekiq-inf-gprd-2019.09.13* --save=results-$now.json
cat results-$now.json | jq
```

Count all occurences of a specific value for a given field name.

```{sh}
bundler exec ./query.rb --index=pubsub-sidekiq-inf-gprd-2019.09.13* --pluck=error_class --count
```

Run a custom query.

```{sh}
cat example_query.json | jq
bundler exec ./query.rb --query=example_query.json --sample --quiet | jq
bundler exec ./query.rb --query=example_query.json --hits-only --no-save --verbose --quiet | jq
bundler exec ./query.rb --query=./example_query.json --sample --index=pubsub-gitaly-inf-gprd-*
```

Query for the latest entry in any gitaly index, without saving.

```{sh}
bundler exec ./query.rb --index='pubsub-gitaly-inf-gprd*' --limit=1 --hits-only --no-save --quiet | jq
```
